<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Client;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

class ClientController extends FOSRestController
{
    /**
     * @Rest\Get("/api/check_auth")
     * Check Authentification user logged
     * This function return a boolean.
     */
	public function checkAuthentification(){
		//Get the data user logged
		$user = $this->get('security.token_storage')->getToken()->getUser();
		//Check if are logged asking if the variable $user is an object
		if (is_object($user)){
			//If token is empty, we created and update a new token in the user.
			if ($user->getConfirmationToken() == ''){
				$tokenGenerator = $this->container->get('fos_user.util.token_generator');
				$user->setConfirmationToken($tokenGenerator->generateToken());
				$sn = $this->getDoctrine()->getManager();
				$sn->flush();
			}
			return $user;
		}else{
			return false;
		}
	}

	/**
	* Function to get the ConfrimationToken by token for the rest api to send by web service
	*/
    public function findUserByConfirmationToken($token)
    {
        $em = $this->getDoctrine()->getManager();

        $RAW_QUERY = 'SELECT * FROM fos_user where confirmation_token = "'. $token .'"';
        
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();

        $result = $statement->fetchAll();

        return $result;
    }

    /**
     * @Rest\Get("/api/client")
     * Get All Clients
     * This function return a array with data of client or a view with a message a message for not found
     * METHOD: GET
     */
    public function getAction(Request $request)
    {
  		$restresult = $this->getDoctrine()->getRepository('AppBundle:Client')->findAll();
		$user = $this->checkAuthentification();
		//Check If token sended exist in an user
		$token = $request->get('token');
		$result_token = $this->findUserByConfirmationToken($token);
		//Check Authentification		
		if ($user || $result_token){
			//If data no found in BD
			if ($restresult === null) {
			  return new View("No existe ningun cliente", Response::HTTP_NOT_FOUND);
			}
        	return $restresult;
		}else{
			return new View("No tiene permisos para acceder aqui. Token ingresado invalido", Response::HTTP_NOT_FOUND);		
		}

    }

	/**
	* @Rest\Get("/api/client/{id}")
	* Get Data Client By Id
	* This function return a array of data of client or a view with a message for not found
	* METHOD: GET
	*/
	public function idAction($id, Request $request)
	{
		$user = $this->checkAuthentification();
		//Check If token sended exist in an user
		$token = $request->get('token');
		$result_token = $this->findUserByConfirmationToken($token);
		//Check Authentification		
		if ($user || $result_token){
			$singleresult = $this->getDoctrine()->getRepository('AppBundle:Client')->find($id);
			//If data no found in BD
			if ($singleresult === null) {
				return new View("Cliente no encontrado", Response::HTTP_NOT_FOUND);
			}
			return $singleresult;
		}else{
			return new View("No tiene permisos para acceder aqui. Token ingresado invalido", Response::HTTP_NOT_FOUND);	
		}
	}

	/**
	* @Rest\Post("/api/client")
	* Add New Client
	* This function return views with messages: validation of name o lastname or success message.
	* You must pass this parameters: 
		name (string), 
		lastname(string), 
		dni(string), 
		email (string), 
		token(string)
	* METHOD: POST
	*/
	public function postAction(Request $request)
	{
		$data = new Client;
		$name = $request->get('name');
		$lastname = $request->get('lastname');
		$dni = $request->get('dni');
		$email = $request->get('email');
		$token = $request->get('token');

		//Check Required values
		if(empty($name) || empty($lastname)){
			return new View("Los campos name y lastname NO PUEDEN SER NULOS", Response::HTTP_NOT_ACCEPTABLE); 
		}

		$user = $this->checkAuthentification();
		//Check If token sended exist in an user
		$token = $request->get('token');
		$result_token = $this->findUserByConfirmationToken($token);
		//Check Authentification		
		if ($user || $result_token){
			$data->setName($name);
			$data->setLastname($lastname);
			$data->setDni($dni);
			$data->setEmail($email);
			$em = $this->getDoctrine()->getManager();
			$em->persist($data);
			$em->flush();

			return new View("Cliente Añadido Correctamente", Response::HTTP_OK);
		}else{
			return new View("No tiene permisos para acceder aqui. Token ingresado invalido", Response::HTTP_NOT_FOUND);	
		}
	}

	/**
	 * @Rest\Put("/api/client/{id}")
	 * Update Data Client By Id
	 * This function return a views with messages especified.
	 * You must pass this parameters: 
		name (string), 
		lastname(string), 
		dni(string), 
		email (string), 
		token(string)
	 * METHOD: PUT
	*/
	public function updateAction($id, Request $request)
	{ 
		$data = new Client;
		$name = $request->get('name');
		$lastname = $request->get('lastname');
		$dni = $request->get('dni');
		$email = $request->get('email');
		$token = $request->get('token');

		$sn = $this->getDoctrine()->getManager();
		$client = $this->getDoctrine()->getRepository('AppBundle:Client')->find($id);

		$user = $this->checkAuthentification();
		//Check If token sended exist in an user
		$token = $request->get('token');
		$result_token = $this->findUserByConfirmationToken($token);
		//Check Authentification		
		if ($user || $result_token){
			$msg = '';
			if (empty($client)) {
				return new View("Cliente no encontrado", Response::HTTP_NOT_FOUND);
			} 
			else{
				if(!empty($name)){
					$client->setName($name);
					$sn->flush();
					$msg .= ' name ';
				}
				if(!empty($lastname)){
					$client->setLastname($lastname);
					$sn->flush();
					$msg .= ' lastname ';
				}
				if(!empty($dni)){
					$client->setDni($dni);
					$sn->flush();
					$msg .= ' dni ';
				}
				if(!empty($email)){
					$client->setEmail($email);
					$sn->flush();
					$msg .= ' email ';
				}
				return new View("$msg de Cliente Modificado Correctamente", Response::HTTP_OK);
			}
		}else{
			return new View("No tiene permisos para acceder aqui. Token ingresado invalido", Response::HTTP_NOT_FOUND);	
		}
	}

	/**
	 * @Rest\Delete("/api/client/{id}")
 	 * Delete Client By Id
 	 * METHOD: DELETE
 	*/
	public function deleteAction($id, Request $request)
	{

		$user = $this->checkAuthentification();
		//Check If token sended exist in an user
		$token = $request->get('token');
		$result_token = $this->findUserByConfirmationToken($token);
		//Check Authentification		
		if ($user || $result_token){				
			$data = new Client;
			$sn = $this->getDoctrine()->getManager();
			$client = $this->getDoctrine()->getRepository('AppBundle:Client')->find($id);

			if (empty($client)) {
				return new View("Cliente no encontrado", Response::HTTP_NOT_FOUND);
			}
			else {
				$sn->remove($client);
				$sn->flush();
			}

			return new View("Cliente Eliminado Correctamente", Response::HTTP_OK);
		}else{
			return new View("No tiene permisos para acceder aqui. Token ingresado invalido", Response::HTTP_NOT_FOUND);	
		}

	}

}


?>